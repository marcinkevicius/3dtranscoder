/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: kid
 *
 * Created on February 7, 2016, 10:59 PM
 */
#include "H264_Decoder.h"
#include <cstdlib>
#include "opencv2/opencv.hpp"
#include "NvEncoder.h"
#include "/usr/local/cuda-7.5/nvidia_video_sdk_6.0.1/Samples/common/inc/nvEncodeAPI.h"
#include "/usr/local/cuda-7.5/nvidia_video_sdk_6.0.1/Samples/common/inc/nvUtils.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/avutil.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
}

#define BITSTREAM_BUFFER_SIZE 2 * 1024 * 1024

using namespace std;
using namespace cv;

/*
 * 
 */

Mat anagl_result; // final anaglymp result
Mat m;
AVFrame dst;
struct SwsContext *convert_ctx;
Mat        planes[3], fliped_image, original_3D(1080, 896, CV_8UC3);
int from_to[] = { 0,0, 1,1, 2,2 };

int nvec_encode(Mat cv_mat, CNvEncoder *nv_encoder, int *numFramesEncoded) {
    
    Mat yuvImage, chan[3];
    uint8_t *yuv[3];
    
    cvtColor(cv_mat,yuvImage,CV_BGR2YUV_I420);
//    nv_encoder.loadframe(nv_encoder.yuv, yuvImage.data, cv_mat.size().width, cv_mat.size().height);
    //split(yuvImage, chan);
    
    
    yuv[0] = new uint8_t[nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height];
    std::copy(
            yuvImage.data, 
            yuvImage.data+(nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height), 
            yuv[0]);
    
    yuv[1] = new uint8_t[nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height / 4];
    std::copy(
            yuvImage.data+(nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height), 
            yuvImage.data+(int)((nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height) * 1.25),
            yuv[1]);
    
    yuv[2] = new uint8_t[nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height / 4];
    std::copy(
            yuvImage.data+(int)((nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height) * 1.25),
            yuvImage.data+(int)((nv_encoder->encodeConfig.width * nv_encoder->encodeConfig.height) * 1.5),
            yuv[2]);

    EncodeFrameConfig stEncodeFrame;
    memset(&stEncodeFrame, 0, sizeof(stEncodeFrame));
    stEncodeFrame.yuv[0] = yuv[0];//chan[0].data;//nv_encoder.yuv[0];
    stEncodeFrame.yuv[1] = yuv[1];//chan[1].data;//nv_encoder.yuv[1];
    stEncodeFrame.yuv[2] = yuv[2];//chan[2].data;//nv_encoder.yuv[2];

    stEncodeFrame.stride[0] = nv_encoder->encodeConfig.width;
    stEncodeFrame.stride[1] = ( nv_encoder->encodeConfig.isYuv444 ) ? nv_encoder->encodeConfig.width : nv_encoder->encodeConfig.width / 2;
    stEncodeFrame.stride[2] = ( nv_encoder->encodeConfig.isYuv444) ? nv_encoder->encodeConfig.width : nv_encoder->encodeConfig.width / 2;
    stEncodeFrame.width = nv_encoder->encodeConfig.width;
    stEncodeFrame.height = nv_encoder->encodeConfig.height;

    nv_encoder->EncodeFrame(&stEncodeFrame, false, nv_encoder->encodeConfig.width, nv_encoder->encodeConfig.height);
    (*numFramesEncoded)++;
    return *numFramesEncoded;
};

Mat mergeTo3d(Mat m){
    Mat d3_result;
    split(m,planes);
    flip(planes[2](Rect(896,0,896,1080)), fliped_image,-1);
    Mat in[] = {planes[0](Rect(0,0,896,1080)), planes[1](Rect(0,0,896,1080)), fliped_image};
    mixChannels( in , 3, &original_3D, 1, from_to, 3 );
    flip(original_3D, original_3D,1); //flip around x axis, (need this?))
    transpose(original_3D, d3_result);
    //cout << "cols " << d3_result.cols << " rows " << d3_result.rows << "\n";
    return d3_result;
};

int main(int argc, char** argv) {

    CNvEncoder nvEncoder;
    nvEncoder.EncodeMain();
    int framecount=0;
    boost::posix_time::ptime t1 = boost::posix_time::second_clock::local_time();
    boost::posix_time::ptime t2 = boost::posix_time::second_clock::local_time();

    H264_Decoder * ddec = new H264_Decoder();
    ddec->load("/home/kid/Desktop/buffer");
    while(ddec->readFrame()){
        if (ddec->cb_frame) {
            ddec->cb_frame=false;
            int w = ddec->picture->width;
            int h = ddec->picture->height;
            m = cv::Mat(h, w, CV_8UC3);
            dst.data[0] = (uint8_t *)m.data;
            av_image_fill_arrays(dst.data, dst.linesize, dst.data[0], AV_PIX_FMT_BGR24, w, h, 1);
            
            enum AVPixelFormat src_pixfmt = (enum AVPixelFormat)ddec->picture->format;
            
            enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

            convert_ctx = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);
            
            if(convert_ctx == NULL) {
                fprintf(stderr, "Cannot initialize the conversion context!\n");
                exit(1);
            }
            sws_scale(convert_ctx, ddec->picture->data, ddec->picture->linesize, 0, h, dst.data, dst.linesize);
            
            anagl_result=mergeTo3d(m);
            
            imshow("MyVideo", anagl_result);
            nvec_encode(anagl_result, &nvEncoder, &framecount);
            waitKey(1);

        }
        waitKey(1);
        t2 = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_duration diff = t2 - t1;
        std::cout << "tim diff -> " << diff.total_milliseconds() << std::endl;
        t1 = boost::posix_time::second_clock::local_time();
    }
    nvEncoder.EncodeMainEnd();

    return 0;
}

