#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/H264_Decoder.o \
	${OBJECTDIR}/NvEncoder.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=/usr/local/lib/libavcodec.so /usr/local/lib/libavutil.so /usr/local/lib/libswscale.so /usr/local/lib/libswresample.so /usr/local/lib/libopencv_calib3d.so /usr/local/lib/libopencv_core.so /usr/local/lib/libopencv_cudaarithm.so /usr/local/lib/libopencv_cudabgsegm.so /usr/local/lib/libopencv_cudacodec.so /usr/local/lib/libopencv_cudafeatures2d.so /usr/local/lib/libopencv_cudafilters.so /usr/local/lib/libopencv_cudaimgproc.so /usr/local/lib/libopencv_cudalegacy.so /usr/local/lib/libopencv_cudaobjdetect.so /usr/local/lib/libopencv_cudaoptflow.so /usr/local/lib/libopencv_cudastereo.so /usr/local/lib/libopencv_cudawarping.so /usr/local/lib/libopencv_cudev.so /usr/local/lib/libopencv_features2d.so /usr/local/lib/libopencv_flann.so /usr/local/lib/libopencv_highgui.so /usr/local/lib/libopencv_imgcodecs.so /usr/local/lib/libopencv_imgproc.so /usr/local/lib/libopencv_ml.so /usr/local/lib/libopencv_objdetect.so /usr/local/lib/libopencv_photo.so /usr/local/lib/libopencv_shape.so /usr/local/lib/libopencv_stitching.so /usr/local/lib/libopencv_superres.so /usr/local/lib/libopencv_ts.a /usr/local/lib/libopencv_video.so /usr/local/lib/libopencv_videoio.so /usr/local/lib/libopencv_videostab.so -lboost_thread -lboost_system /usr/lib/x86_64-linux-gnu/libdl.so

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libavcodec.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libavutil.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libswscale.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libswresample.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_calib3d.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_core.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudaarithm.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudabgsegm.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudacodec.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudafeatures2d.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudafilters.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudaimgproc.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudalegacy.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudaobjdetect.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudaoptflow.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudastereo.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudawarping.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_cudev.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_features2d.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_flann.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_highgui.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_imgcodecs.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_imgproc.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_ml.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_objdetect.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_photo.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_shape.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_stitching.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_superres.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_ts.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_video.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_videoio.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/local/lib/libopencv_videostab.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: /usr/lib/x86_64-linux-gnu/libdl.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder ${OBJECTFILES} ${LDLIBSOPTIONS} NvHWEncoder.o dynlink_cuda.o -L/usr/lib64/nvidia -lnvidia-encode -ldl

${OBJECTDIR}/H264_Decoder.o: H264_Decoder.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/H264_Decoder.o H264_Decoder.cpp

${OBJECTDIR}/NvEncoder.o: NvEncoder.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NvEncoder.o NvEncoder.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3dtranscoder

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
